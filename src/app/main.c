/*************************************************************************
  main.c
  HKUST Smartcar 2013 Motor Deriver and UART Testing Program

  Authoured by:
  John Ching
  Louis Mo
  Yumi Chan

  Hardware By:
  Zyan Shao

  Hardware Environment : 3rd Gen Sensor Group Main Board
*************************************************************************/

/*************** Program Work 24/06/2013***************/

#include "include.h"
#include "common.h"
u32 rightservorange = 750+130;
u32 leftservorange = 750-130;

u8 checksensors(u8 leftmost, u8 left, u8 right, u8 rightmost){
  u8 a=1,b=1,c=1,d=1;
  if(leftmost!=X) a= gpio_get(PORTC,4)&&leftmost;
  if(left!=X) b= gpio_get(PORTC,5)&&left;
  if(right!=X) c= gpio_get(PORTC,6)&&right;
  if(rightmost!=X) d=gpio_get(PORTC,7)&&rightmost;
  return a&&b&&c&&d;
}
u32 limitrange(u32 value){
  if(value > rightservorange) return rightservorange;
  else if(value < leftservorange) return leftservorange;
  else return value;
}
void printifchange(char newstring[1000], u32 newvariables){
  static char oldstring[1000];
  static u32 oldvariables;
  if(oldstring!=newstring || oldvariables!=newvariables){
    printf(newstring,newvariables);
  }
}
void main(void)
{
        
        uart_init(UART3, 115200); // For our flashed bluetooth
        //uart_init(UART3, 9600); // For non-flashed bluetooth
        
        /////init sensors/////
        gpio_init(PORTC,4,GPO,1);
        gpio_init(PORTC,5,GPO,1);
        gpio_init(PORTC,6,GPO,1);
        gpio_init(PORTC,7,GPO,1);
        
        
        /////init servo/////
        u32 initmiddle=750;
        u32 multiplier = 3;
        u32 left=113;
        u32 right=113;      
        u32 currentservo;
        FTM_PWM_init(FTM2,CH0,50,0);
        FTM_PWM_Duty(FTM2,CH0,initmiddle);
        
        u32 servodirection[5]={
          initmiddle,
          initmiddle-left,
          initmiddle-multiplier*left,
          initmiddle+right, 
          initmiddle+multiplier*right
        };
        
        /////init motor speed////
        u32 initspeed=4000;
        u32 currentspeed;
        FTM_PWM_init(FTM1,CH0,10000,0);
        FTM_PWM_Duty(FTM1,CH0,initspeed);
        
        u32 motorspeeds[4]={
        0,
        initspeed+145,
        initspeed-755,
        initspeed-765,
        };
        
        /////init motor direction/////
        u8 direction = 0;
        gpio_init(PORTD,7,GPO,1);
        
        /////set motor direction/////
        gpio_set(PORTD,7, direction);
        
        
        /////define states/////
        u8 states[maxnoofstates][4] = {
          {1,1,1,1},
          {0,0,0,0},
          {X,X,1,1},
          {1,1,X,X},
          {X,X,X,1},
          {1,X,X,X},
          {X,X,X,X},
        };
        u8 statesetting[maxnoofstates][2] = {
          {0,0},
          {0,1},
          {2,3},
          {4,3},
          {1,2},
          {3,2},
          {0,1},
        };
        
       
        //u8 choice = 0;
        //u8 locked = 0;

        
        //u8 oldservo =0;
        //u8 oldmotor =0;
        
        while(1){
          /*printf("Please choose:\n");

          printf("1. Change speed\n");
          printf("2. Change servo left\n");
          printf("3. Change servo right\n");
          printf("4. lock\n");
          choice = uart_getchar(UART3);
          switch(choice){
          case '1':
            initspeed += uart_getchar(UART3)-'0';
            printifchange("Motor: %d",initspeed);
            break;
          case '2':
            left += uart_getchar(UART3)-'0';
            printifchange("Servo: %d",currentservo);
            break;
          case '3':
            right += uart_getchar(UART3)-'0';
            printifchange("Servo: %d",currentservo);
            break;
          case '4':
            locked = !locked;
            printifchange("Locked: %d",locked);
            break;
          default:
            while(!locked){*/
              
              for(u8 i=0; i<maxnoofstates; i++){
                if(checksensors(states[i][0],states[i][1],states[i][2],states[i][3])){
                  currentservo = limitrange(servodirection[statesetting[i][0]]);
                  currentspeed = motorspeeds[statesetting[i][1]];
                  FTM_PWM_Duty(FTM2,CH0,currentservo);
                  FTM_PWM_Duty(FTM1,CH0,currentspeed);
                  printifchange("Servo: %d\n",currentservo);              
                  printifchange("Motor: %d\n",currentspeed);
                  //choice = uart_getchar(UART3);
                  break;  
                }
              }
              
            /*}
            break;
          }*/
        }
        
  
        

}
